import csv
import gzip
import json
import logging
from pathlib import Path
from typing import Generator

import psycopg
from fiona.io import ZipMemoryFile
from tqdm.auto import tqdm

logger = logging.getLogger(__name__)


def create_schema(cur: psycopg.cursor) -> None:
    sql = """
        DROP SCHEMA IF EXISTS fra CASCADE;
        CREATE SCHEMA fra;

        CREATE TABLE fra.addresses (
            id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
            _created_at timestamp DEFAULT current_timestamp,
            _updated_at timestamp DEFAULT current_timestamp,
            code_insee VARCHAR(5) NOT NULL,
            point_4326 Geometry(Point, 4326) NOT NULL CONSTRAINT geom_is_valid CHECK (
                ST_IsEmpty(point_4326) IS FALSE
                AND ST_SRID(point_4326) = 4326
                AND ST_IsValid(point_4326)
            )
        );

        CREATE TABLE fra.municipalities (
            id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
            _created_at timestamp DEFAULT current_timestamp,
            _updated_at timestamp DEFAULT current_timestamp,
            code_insee VARCHAR(5) NOT NULL,
            geometry_4326 Geometry(MultiPolygon, 4326) NOT NULL CONSTRAINT geom_is_valid CHECK (
                ST_IsEmpty(geometry_4326) IS FALSE
                AND ST_SRID(geometry_4326) = 4326
                AND ST_IsValid(geometry_4326)
            )
        );
        CREATE TEMPORARY TABLE municipalities_tmp (
            code_insee VARCHAR(5) NOT NULL,
            geometry_4326_geojson_str TEXT NOT NULL
        );
    """
    cur.execute(sql)


def get_municipalities_features() -> Generator[dict, None, None]:
    with ZipMemoryFile(
        open(Path(__file__).parent / "data/commune-frmetdrom.zip", "rb").read()
    ) as zip:
        with zip.open("COMMUNE_FRMETDROM.shp") as f:
            for i, feat in enumerate(f):
                yield feat


def get_address_rows() -> Generator[dict, None, None]:
    with gzip.open(
        Path(__file__).parent / "data/adresses-france.csv.gz", mode="rt", newline=""
    ) as f:
        reader = csv.DictReader(f, delimiter=";")
        for row in reader:
            yield row


if __name__ == "__main__":
    uri = "postgres://postgres:password@0.0.0.0:6544/postgres"
    with psycopg.connect(uri, row_factory=psycopg.rows.dict_row) as conn:
        with conn.cursor() as cur:
            create_schema(cur)
            with cur.copy(
                "COPY fra.addresses (code_insee, point_4326) FROM STDIN"
            ) as copy:
                for row in tqdm(get_address_rows(), desc="Copying addresses"):
                    copy.write_row(
                        [
                            row["code_insee"],
                            f"""Point({row["lon"]} {row["lat"]})""",
                        ]
                    )
            conn.commit()

            with cur.copy("COPY municipalities_tmp FROM STDIN") as copy:
                logger.info("Loading data in temporary table")
                for feat in tqdm(
                    get_municipalities_features(), desc="Copying territories"
                ):
                    copy.write_row(
                        [
                            feat["properties"]["INSEE_COM"],
                            json.dumps(dict(feat["geometry"])),
                        ]
                    )

            logger.info("Inserting municipalities in final table")
            cur.execute(
                """
                INSERT INTO fra.municipalities (
                    code_insee,
                    geometry_4326
                )
                SELECT
                    code_insee,
                    ST_Multi(
                        ST_MAKEVALID(
                            ST_GeomFromGeoJSON(geometry_4326_geojson_str)
                        )
                    )
                FROM municipalities_tmp
            """
            )

        conn.commit()
